import { signIn, signOut, useSession } from "next-auth/react";
import { api } from "../utils/api";
import { type FC, useState } from "react";
import build from "next/dist/build";

const Home = () => {
  const { data: session, status } = useSession();

  if ( status === "loading" ) {
    return <main className="flex flex-col items-center pt-4">Loading...</main>
  }

  return (
    <main className="flex flex-col items-center">
    <h1 className="text-3xl pt-4">Guestbook</h1>
      <p>Simple Guestbook App build with <code>t3-app</code></p>
      <div className="pt-10">
        <div>
    {
      session ? (
          <>
          <p className="mb-4 text-center">Hi {session.user?.name}</p>
          <button 
            type="button"
            className="mx-auto block rounded-md bg-neutral-800 text-center py-3 px-6 hover:bg-neutral-700"
      onClick={() => {
            signOut().catch(console.log);
            }}>
            Logout
          </button>
              <div className="pt-6">
              <Form />
            </div>
        </>
      ) : (
        <button 
            type="button"
          className="mx-auto block rounded-md text-center px-6 py-3 bg-neutral-800 hover:bg-neutral-700"
      onClick={() => {
          signIn("discord").catch(console.log);
          }}>
          Login with Discord
        </button>

        )}
          <div className="pt-10">
            <GuestbookEntries />
          </div>
        </div>
    </div>
    </main>
  );
};


const GuestbookEntries:FC = () => {
  const { data: guestbookEntries, isLoading } = api.guestbook.getAll.useQuery();

  if(isLoading) return <div>Fetching Messages...</div>;

  return (
    <div className="flex flex-col gap-4">
      {guestbookEntries?.map((entry, index) => {
      return (
        <div key={index}>
          <span>{entry.name}</span>
          <p>{entry.message}</p>
        </div>
      );
      })}
    </div>
  );
};


const Form:FC = () => {
  const [message, setMessage] = useState("");
  const {data: session, status} = useSession();

  const utils = api.useContext();
  const postMessage = api.guestbook.postMessage.useMutation({
    onMutate: async (newEntry) => {
      await utils.guestbook.getAll.cancel();
      utils.guestbook.getAll.setData( undefined, (prevEntries) => {
        if(prevEntries){
          return [newEntry, ...prevEntries];
        } else {
          return [newEntry];
        }
      });
    },
    onSettled: async () => {
      await utils.guestbook.getAll.invalidate();
    },
  });



  return status === "authenticated" ? (
    <form
      className="flex gap-2"
      onSubmit={(event) => {
        event.preventDefault();
        postMessage.mutate({
          name: session.user?.name as string,
          message,
        });
        setMessage("");
      }}
    >

      <input
        type="text"
        className="rounded-md border-2 border-zinc-800 px-4 py-2 bg-neutral-900 focus:outline-none"
        placeholder="Your message..."
        minLength={2}
        maxLength={100}
        value={message}
        onChange={(event) => setMessage(event.target.value)}
      />

      <button
        type="submit"
        className="rounded-none-md border-2 border-zinc-800 p-2 focus:outline-none"
      >
        Submit
      </button>
    </form>
    ) : null ;
  };

export default Home;
